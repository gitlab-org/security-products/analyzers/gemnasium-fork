package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner"
	scannercli "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/cli"
)

func analyzeFlags() []cli.Flag {
	return append(scannercli.ClientFlags(), scannercli.FinderFlags()...)
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {

	// find compatible files
	finder := scannercli.NewFinder(c)
	files, err := finder.FindFiles(path)
	if err != nil {
		return nil, err
	}

	// get sources of dependencies
	sources, err := scanner.ParseFiles(files)
	if err != nil {
		return nil, err
	}

	// get advisories
	client, err := scannercli.NewClient(c)
	if err != nil {
		return nil, err
	}
	advisories, err := client.Advisories(sources.Packages())
	if err != nil {
		return nil, err
	}

	// return affected sources
	var output bytes.Buffer
	result := scanner.AffectedSources(advisories, sources...)
	enc := json.NewEncoder(&output)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}
