package scanner

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

// Finder finds supported dependency files.
type Finder struct {
	IgnoredDirs []string
}

// FindFiles scans a directory and return supported dependency files.
func (f Finder) FindFiles(root string) ([]File, error) {
	files := []File{}
	search := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Get relative path
		rel, err := filepath.Rel(root, path)
		if err != nil {
			return err
		}

		// Skip excluded directories
		for _, name := range filepath.SplitList(rel) {
			for _, ignored := range f.IgnoredDirs {
				if name == ignored {
					return filepath.SkipDir
				}
			}
		}

		// Look for compatible parser
		if p := parser.Lookup(info.Name()); p != nil {
			files = append(files, File{Path: path, RelativePath: rel, Parser: *p})
		}

		return nil
	}

	// Walk the directory
	err := filepath.Walk(root, search)
	return files, err
}
