package scanner

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

const getAdvisoriesPath = "/advisories/q"

var getAdvisoriesRelURL *url.URL

// NewClient initializes a client with a http.Client and server URL
func NewClient(cl *http.Client, serverURL *url.URL) *Client {
	return &Client{cl, serverURL}
}

// Client is used to get advisories from the server.
type Client struct {
	httpClient *http.Client
	serverURL  *url.URL
}

// ErrUnexpectedStatus is raised when the server returns an unexpected status code.
type ErrUnexpectedStatus struct {
	StatusCode int
}

// Error returns the error message.
func (e ErrUnexpectedStatus) Error() string {
	return fmt.Sprintf("Unexpected server status: %d", e.StatusCode)
}

// Advisories connects to the server and lists all the security advisories
// for the given packages.
func (cl Client) Advisories(pkgs []Package) ([]Advisory, error) {
	// advisories URL
	u := cl.serverURL.ResolveReference(getAdvisoriesRelURL)

	// encode packages list
	buf := new(bytes.Buffer)
	if err := json.NewEncoder(buf).Encode(pkgs); err != nil {
		return nil, err
	}

	// POST request
	resp, err := cl.httpClient.Post(u.String(), "", buf)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// check status
	if c := resp.StatusCode; c < 200 || c > 299 {
		return nil, ErrUnexpectedStatus{resp.StatusCode}
	}

	// decode advisories
	result := []Advisory{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}

	return result, nil
}

func init() {
	// parse relative URL for GET advisories API endpoint
	var err error
	getAdvisoriesRelURL, err = url.Parse(getAdvisoriesPath)
	if err != nil {
		panic(err)
	}
}
