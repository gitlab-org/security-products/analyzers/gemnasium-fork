package scanner

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"

// File describes a dependency file supported by a parser.
// It is fond by the Finder and parsed by ParseFiles.
type File struct {
	Path         string // Path is the absolute path.
	RelativePath string // RelativePath is path relative to the dir scanned by the Finder.
	Parser       parser.Parser
}
