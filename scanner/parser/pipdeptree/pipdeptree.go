package pipdeptree

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

type Package struct {
	Name    string `json:"package_name"`
	Key     string `json:"key"`
	Version string `json:"installed_version"` // Installed version
}

func Parse(r io.Reader) ([]parser.Dependency, error) {
	var document = []struct {
		Package `json:"package"`
	}{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, err
	}

	pdeps := make([]parser.Dependency, len(document))
	for i, pkg := range document {
		pdeps[i] = parser.Dependency{Name: pkg.Name, Version: pkg.Version}
	}
	return pdeps, nil
}

func init() {
	parser.Register("pipdeptree", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePypi,
		Filenames:   []string{"pipdeptree.json"},
	})
}
