package npm

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

type Document struct {
	LockfileVersion int64 `json:"lockfileVersion"`
	DependencyNode
}

type Dependency struct {
	Version string `json:"version"` // Installed version
	Line    int64  `json:"line"`
	DependencyNode
}

type DependencyNode struct {
	Dependencies map[string]Dependency `json:"dependencies"` // Nested dependencies
}

const supportedFileFormatVersion = 1

func Parse(r io.Reader) ([]parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, err
	}
	if document.LockfileVersion != supportedFileFormatVersion {
		return nil, parser.ErrWrongFileFormatVersion
	}

	return parseNode(document.DependencyNode)
}

func init() {
	parser.Register("npm", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNpm,
		Filenames:   []string{"package-lock.json"},
	})
}

func parseNode(dn DependencyNode) ([]parser.Dependency, error) {
	pdeps := []parser.Dependency{}
	for name, dep := range dn.Dependencies {
		pdeps = append(pdeps, parser.Dependency{Name: name, Version: dep.Version})
		subdeps, err := parseNode(dep.DependencyNode)
		if err != nil {
			return nil, err
		}
		pdeps = append(pdeps, subdeps...)
	}
	return pdeps, nil
}
