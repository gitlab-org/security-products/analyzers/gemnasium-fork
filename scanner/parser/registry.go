package parser

import (
	"sort"
	"sync"
)

var parsersMu sync.Mutex
var parsers = make(map[string]Parser)

// Register registers a parser with a name.
func Register(name string, parser Parser) {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	if _, dup := parsers[name]; dup {
		panic("Register called twice for name " + name)
	}
	parsers[name] = parser
}

// Lookup looks for a parser compatible with the given filename.
func Lookup(filename string) *Parser {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	for _, p := range parsers {
		for _, f := range p.Filenames {
			if f == filename {
				return &p
			}
		}
	}
	return nil
}

// Parsers returns a sorted list of the names of the registered parsers.
func Parsers() []string {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	var list []string
	for name := range parsers {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}
