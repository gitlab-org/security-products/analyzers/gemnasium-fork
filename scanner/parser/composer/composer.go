package composer

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

type Document struct {
	Packages []Package `json:"packages"`
}

type Package struct {
	Name    string `json:"name"`
	Version string `json:"version"` // Installed version
}

func Parse(r io.Reader) ([]parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, err
	}

	pdeps := []parser.Dependency{}
	for _, p := range document.Packages {
		pdeps = append(pdeps, parser.Dependency{Name: p.Name, Version: p.Version})
	}
	return pdeps, nil
}

func init() {
	parser.Register("composer", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePackagist,
		Filenames:   []string{"composer.lock"},
	})
}
