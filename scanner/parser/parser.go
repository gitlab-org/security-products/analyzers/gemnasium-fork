package parser

import (
	"errors"
	"io"
)

type ParseFunc func(io.Reader) ([]Dependency, error)

type PackageType string

const (
	PackageTypeGem       = "gem"
	PackageTypeMaven     = "maven"
	PackageTypeNpm       = "npm"
	PackageTypePackagist = "packagist"
	PackageTypePypi      = "pypi"
)

type Parser struct {
	Parse       ParseFunc
	PackageType PackageType
	Filenames   []string
}

type Dependency struct {
	Name    string `json:"name"`    // Package name
	Version string `json:"version"` // Installed version
}

var ErrWrongFileFormatVersion = errors.New("Wrong file format version")
