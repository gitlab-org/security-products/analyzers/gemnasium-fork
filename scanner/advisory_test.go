package scanner

import (
	"bytes"
	"encoding/json"
	"testing"

	"gopkg.in/guregu/null.v3"
)

func TestAdvisory(t *testing.T) {
	t.Run("Affects", func(t *testing.T) {
		advisory := Advisory{AffectedVersions: []string{"0.9.0", "0.8.0"}}

		t.Run("Affected", func(t *testing.T) {
			number := "0.8.0"
			if !advisory.Affects(number) {
				t.Errorf("Expected %s to be affected by %v", number, advisory)
			}
		})

		t.Run("Healthy", func(t *testing.T) {
			number := "1.0.0"
			if advisory.Affects(number) {
				t.Errorf("Expected %s to be NOT affected by %v", number, advisory)
			}
		})
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		advisory := Advisory{
			UUID:             "7d9ba955-fd99-4503-936e-f6833768f76e",
			Category:         null.StringFrom("critical"),
			Identifier:       null.StringFrom("SEC-XXX"),
			Title:            "Regular Expression Denial of Service",
			Description:      "The primary function, minimatch(path, pattern) is vulnerable to ReDoS in the pattern parameter.",
			DisclosureDate:   "2016-06-20",
			FixedVersions:    []string{"3.0.2"},
			AffectedVersions: []string{"2.0.10", "3.0.0"},
			Solution:         null.StringFrom("Upgrade to latest version."),
			Credit:           null.StringFrom("Nick Starke"),
			Links:            []string{"https://nodesecurity.io/advisories/118"},
			Package: Package{
				Type: "npm",
				Name: "minimatch",
			},
		}

		// marshal
		b, err := json.Marshal(&advisory)
		if err != nil {
			t.Fatal(err)
		}

		// indent
		var out bytes.Buffer
		json.Indent(&out, b, "", "      ")

		// compare
		got := out.String()
		want := `{
      "uuid": "7d9ba955-fd99-4503-936e-f6833768f76e",
      "category": "critical",
      "identifier": "SEC-XXX",
      "title": "Regular Expression Denial of Service",
      "description": "The primary function, minimatch(path, pattern) is vulnerable to ReDoS in the pattern parameter.",
      "date": "2016-06-20",
      "fixed_versions": [
            "3.0.2"
      ],
      "affected_versions": [
            "2.0.10",
            "3.0.0"
      ],
      "solution": "Upgrade to latest version.",
      "credit": "Nick Starke",
      "urls": [
            "https://nodesecurity.io/advisories/118"
      ],
      "package": {
            "type": "npm",
            "name": "minimatch"
      }
}`
		if got != want {
			t.Errorf("Wrong result. Expecting:\n%s\nbut got\n%s", got, want)
		}
	})
}
