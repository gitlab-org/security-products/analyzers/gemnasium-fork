package scanner

import "gopkg.in/guregu/null.v3"

// Advisory is a security advisory published for a package.
type Advisory struct {
	UUID             string      `json:"uuid"`
	Category         null.String `json:"category"`
	Identifier       null.String `json:"identifier"`
	Title            string      `json:"title"`
	Description      string      `json:"description"`
	DisclosureDate   string      `json:"date"`
	FixedVersions    []string    `json:"fixed_versions"`
	AffectedVersions []string    `json:"affected_versions"`
	Solution         null.String `json:"solution"`
	Credit           null.String `json:"credit"`
	Links            []string    `json:"urls"`
	Package          Package     `json:"package"`
}

// Affects tells whether a version is affected.
func (a Advisory) Affects(number string) bool {
	for _, v := range a.AffectedVersions {
		if v == number { // TODO compare using semver
			return true
		}
	}
	return false
}
