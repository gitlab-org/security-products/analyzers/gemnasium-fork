package scanner

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"

	"gopkg.in/guregu/null.v3"
)

const testJSONAdvisories = `[{
      "uuid": "7d9ba955-fd99-4503-936e-f6833768f76e",
      "category": "critical",
      "identifier": "SEC-XXX",
      "title": "Regular Expression Denial of Service",
      "description": "The primary function, minimatch(path, pattern) is vulnerable to ReDoS in the pattern parameter.",
      "date": "2016-06-20",
      "fixed_versions": [
        "3.0.2"
      ],
      "affected_versions": [
        "2.0.10",
        "3.0.0"
      ],
      "solution": "Upgrade to latest version.",
      "credit": "Nick Starke",
      "urls": [
        "https://nodesecurity.io/advisories/118"
      ],
      "package": {
        "type": "npm",
        "name": "minimatch"
      }
    }]`

var testAdvisories = []Advisory{
	Advisory{
		UUID:             "7d9ba955-fd99-4503-936e-f6833768f76e",
		Category:         null.StringFrom("critical"),
		Identifier:       null.StringFrom("SEC-XXX"),
		Title:            "Regular Expression Denial of Service",
		Description:      "The primary function, minimatch(path, pattern) is vulnerable to ReDoS in the pattern parameter.",
		DisclosureDate:   "2016-06-20",
		FixedVersions:    []string{"3.0.2"},
		AffectedVersions: []string{"2.0.10", "3.0.0"},
		Solution:         null.StringFrom("Upgrade to latest version."),
		Credit:           null.StringFrom("Nick Starke"),
		Links:            []string{"https://nodesecurity.io/advisories/118"},
		Package: Package{
			Type: "npm",
			Name: "minimatch",
		},
	},
}

func TestClient(t *testing.T) {
	t.Run("Advisories", func(t *testing.T) {
		reqPkgs := []Package{
			{Type: "npm", Name: "q"},
			{Type: "npm", Name: "minimatch"},
		}

		t.Run("OK", func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// check URI
				wantURI := "/advisories/q"
				if r.RequestURI != wantURI {
					t.Errorf("Wrong URI. Expecting %s but got %s", wantURI, r.RequestURI)
				}

				// check method
				wantMethod := "POST"
				if r.Method != wantMethod {
					t.Errorf("Wrong method. Expecting %s but got %s", wantMethod, r.Method)
				}

				// check body
				gotPkgs := []Package{}
				err := json.NewDecoder(r.Body).Decode(&gotPkgs)
				if err != nil {
					t.Fatal(err)
				}
				if !reflect.DeepEqual(reqPkgs, gotPkgs) {
					t.Errorf("Wrong requested packages. Expecting %+v but got %+v", reqPkgs, gotPkgs)
				}

				// serve advisories
				w.Write([]byte(testJSONAdvisories))
			}))
			defer ts.Close()

			u, err := url.Parse(ts.URL)
			if err != nil {
				t.Fatal(err)
			}

			client := NewClient(http.DefaultClient, u)
			got, err := client.Advisories(reqPkgs)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(testAdvisories, got) {
				t.Errorf("Wrong result. Expecting %+v but got %+v", testAdvisories, got)
			}
		})

		t.Run("InternalServerError", func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(500)
				w.Write([]byte("Internal server error"))
			}))
			defer ts.Close()

			u, err := url.Parse(ts.URL)
			if err != nil {
				t.Fatal(err)
			}

			client := NewClient(http.DefaultClient, u)
			_, err = client.Advisories(reqPkgs)
			wantError := ErrUnexpectedStatus{500}
			if !reflect.DeepEqual(wantError, err) {
				t.Errorf("Wrong error. Expecting %+v but got %+v", wantError, err)
			}
		})
	})
}
