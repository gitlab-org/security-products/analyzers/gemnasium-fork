package scanner

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"

// Source is a source of dependencies. It combines dependencies with a file.
type Source struct {
	FilePath    string              `json:"filepath"`
	PackageType parser.PackageType  `json:"package_type"`
	Deps        []parser.Dependency `json:"dependencies"`
}

// Packages returns all the packages found in the dependencies. It may contain duplicates.
func (s Source) Packages() []Package {
	pkgs := make([]Package, len(s.Deps))
	for i, d := range s.Deps {
		pkgs[i] = Package{Type: s.PackageType, Name: d.Name}
	}
	return pkgs
}

// Sources extends a slice of FileDeps with helper methods.
type Sources []Source

// Packages return the packages for all the dependencies listed in the files.
func (sources Sources) Packages() []Package {
	packages := []Package{}
	for _, s := range sources {
		packages = append(packages, s.Packages()...)
	}
	return packages
}
