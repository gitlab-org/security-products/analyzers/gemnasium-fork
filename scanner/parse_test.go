package scanner

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/yarn"
)

func TestParse(t *testing.T) {
	f, err := os.Open("parser/gemfile/fixtures/simple/Gemfile.lock")
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	got, err := Parse(f, "Gemfile.lock", "xyz")
	if err != nil {
		t.Fatal(err)
	}

	want := &Source{
		FilePath:    "xyz",
		PackageType: "gem",
		Deps: []parser.Dependency{
			{Name: "pg", Version: "1.0.0"},
			{Name: "puma", Version: "2.16.0"},
		},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestParseFiles(t *testing.T) {
	getParser := func(filename string) parser.Parser {
		if p := parser.Lookup(filename); p != nil {
			return *p
		}
		t.Fatal(ErrParserNotFound{filename})
		return parser.Parser{}
	}

	files := []File{
		{
			Path:         "parser/gemfile/fixtures/simple/Gemfile.lock",
			RelativePath: "simple/Gemfile.lock",
			Parser:       getParser("Gemfile.lock"),
		},
		{
			Path:         "parser/yarn/fixtures/simple/yarn.lock",
			RelativePath: "simple/yarn.lock",
			Parser:       getParser("yarn.lock"),
		},
	}

	got, err := ParseFiles(files)
	if err != nil {
		t.Fatal(err)
	}

	want := Sources{
		{
			FilePath:    "simple/Gemfile.lock",
			PackageType: "gem",
			Deps: []parser.Dependency{
				{Name: "pg", Version: "1.0.0"},
				{Name: "puma", Version: "2.16.0"},
			},
		},
		{
			FilePath:    "simple/yarn.lock",
			PackageType: "npm",
			Deps: []parser.Dependency{
				{Name: "acorn", Version: "4.0.4"},
				{Name: "acorn", Version: "3.3.0"},
				{Name: "acorn", Version: "4.0.11"},
				{Name: "@angular/animations", Version: "4.4.6"},
			},
		},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%#v\nbut got:\n%#v", want, got)
	}
}
