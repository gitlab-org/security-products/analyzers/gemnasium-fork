package scanner

import (
	"io"
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

// ErrParserNotFound is raised when parser lookup fails.
type ErrParserNotFound struct {
	Filename string
}

// Error returns the error message.
func (e ErrParserNotFound) Error() string {
	return "No compatible parser for " + e.Filename
}

// Parse parses a reader with a parser capable of handling fileName.
// The FilePath of the returned Source is set to the given filePath.
func Parse(r io.Reader, fileName string, filePath string) (*Source, error) {
	parser := parser.Lookup(fileName)
	if parser == nil {
		return nil, ErrParserNotFound{filePath}
	}

	deps, err := parser.Parse(r)
	if err != nil {
		return nil, err
	}

	return &Source{
		FilePath:    filePath,
		PackageType: parser.PackageType,
		Deps:        deps,
	}, nil
}

// ParseFiles parses the compatible files found by FindFiles.
// The FilePath of the source is set to the relative path of the dependency file.
func ParseFiles(files []File) (Sources, error) {
	sources := make([]Source, len(files))
	for i, file := range files {
		// open file
		f, err := os.Open(file.Path)
		if err != nil {
			return nil, err
		}
		defer f.Close()

		// parse
		deps, err := file.Parser.Parse(f)
		if err != nil {
			return nil, err
		}
		sources[i] = Source{
			FilePath:    file.RelativePath,
			PackageType: file.Parser.PackageType,
			Deps:        deps,
		}
	}
	return Sources(sources), nil
}
