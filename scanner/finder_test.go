package scanner

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/yarn"
)

// finderMatch is used to compare a returned File with an expectation.
// It's needed because File embeds a Parser and Parser.Parse can't be compared.
type finderMatch struct {
	Path         string
	RelativePath string
	PackageType  parser.PackageType
}

func TestFinder(t *testing.T) {
	t.Run("FindFiles", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			finder := Finder{}
			_, err := finder.FindFiles("empty")
			if err == nil {
				t.Fatal("Expected error but got none")
			}
		})

		t.Run("fixtures", func(t *testing.T) {
			finder := Finder{IgnoredDirs: []string{"ignore", "skip"}}
			files, err := finder.FindFiles("fixtures")
			if err != nil {
				t.Fatal(err)
			}

			want := []finderMatch{
				{
					// filename alias
					Path:         "fixtures/bundler/gems.locked",
					RelativePath: "bundler/gems.locked",
					PackageType:  "gem",
				},
				{
					// nested directory
					Path:         "fixtures/nested/bundler/Gemfile.lock",
					RelativePath: "nested/bundler/Gemfile.lock",
					PackageType:  "gem",
				},
				{
					// nested directory
					Path:         "fixtures/nested/yarn/yarn.lock",
					RelativePath: "nested/yarn/yarn.lock",
					PackageType:  "npm",
				},
				{
					Path:         "fixtures/yarn/yarn.lock",
					RelativePath: "yarn/yarn.lock",
					PackageType:  "npm",
				},
			}

			got := make([]finderMatch, len(files))
			for i, f := range files {
				got[i] = finderMatch{
					Path:         f.Path,
					RelativePath: f.RelativePath,
					PackageType:  f.Parser.PackageType,
				}
			}

			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expecting:\n%+v\nbut got:\n%+v", want, got)
			}
		})
	})
}
