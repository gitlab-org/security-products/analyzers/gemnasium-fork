package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

func Match(path string, info os.FileInfo) (bool, error) {
	if p := parser.Lookup(info.Name()); p != nil {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("gemnasium", Match)
}
