module gitlab.com/gitlab-org/security-products/analyzers/gemnasium

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common v1.9.0
	gopkg.in/guregu/null.v3 v3.4.0
)
