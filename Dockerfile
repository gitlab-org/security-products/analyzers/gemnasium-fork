FROM scratch
ADD cacert-2018-10-17.pem /etc/ssl/certs/
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
