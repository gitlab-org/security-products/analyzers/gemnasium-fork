package convert

import (
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser"
)

const (
	scannerID   = "gemnasium"
	scannerName = "Gemnasium"

	identifierTypeGemnasiumUUID  = issue.IdentifierType("gemnasium")
	identifierPrefixGemnasiuUUID = "Gemnasium-"

	gemnasiumURL = "https://deps.sec.gitlab.com"
)

func Convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	// decode output
	var result []scanner.AffectedSource
	err := json.NewDecoder(reader).Decode(&result)
	if err != nil {
		return nil, err
	}

	// process affected files
	issues := []issue.Issue{}
	for _, source := range result {
		issues = append(issues, convertSource(source, prependPath)...)

	}

	return issues, nil
}

func convertSource(source scanner.AffectedSource, prependPath string) []issue.Issue {
	issues := make([]issue.Issue, len(source.Affections))
	for i, affection := range source.Affections {
		c := Converter{
			PrependPath: prependPath,
			Source:      source,
			Advisory:    affection.Advisory,
			Dependency:  affection.Dependency,
		}
		issues[i] = c.Issue()
	}
	return issues
}

// Converter is used to convert an affection in a file to a generic issue.
type Converter struct {
	PrependPath string
	Source      scanner.AffectedSource
	Advisory    scanner.Advisory
	Dependency  parser.Dependency
}

// Issue converts an advisory into a generic issue.
func (c Converter) Issue() issue.Issue {
	return issue.DependencyScanningVulnerability{issue.Issue{
		Category: issue.CategoryDependencyScanning,
		Scanner: issue.Scanner{
			ID:   scannerID,
			Name: scannerName,
		},
		Name:        c.Advisory.Title,
		Description: c.Advisory.Description,
		Severity:    issue.LevelUnknown,
		Solution:    c.Advisory.Solution.String,
		Identifiers: c.identifiers(),
		Links:       issue.NewLinks(c.Advisory.Links...),
		Location: issue.Location{
			File: c.filePath(),
			Dependency: issue.Dependency{
				Package: issue.Package{
					Name: c.Dependency.Name,
				},
				Version: c.Dependency.Version,
			},
		},
	}}.ToIssue()
}

func (c Converter) filePath() string {
	return filepath.Join(c.PrependPath, c.Source.FilePath)
}

func (c Converter) identifiers() []issue.Identifier {
	ids := []issue.Identifier{c.primaryIdentifier()}
	if id := c.Advisory.Identifier; id.Valid {
		if identifier, ok := issue.ParseIdentifierID(id.String); ok {
			ids = append(ids, identifier)
		}
	}
	return ids
}

func (c Converter) primaryIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  identifierTypeGemnasiumUUID,
		Name:  identifierPrefixGemnasiuUUID + c.Advisory.UUID,
		Value: c.Advisory.UUID,
		URL:   c.advisoriesURL(),
	}
}

func (c Converter) advisoriesURL() string {
	ptype := string(c.Source.PackageType)
	name, version := c.Dependency.Name, c.Dependency.Version
	return fmt.Sprintf("%s/packages/%s/%s/versions/%s/advisories", gemnasiumURL, ptype, name, version)
}
