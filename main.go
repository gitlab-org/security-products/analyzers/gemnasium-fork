package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/command"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/plugin"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/composer"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/scanner/parser/yarn"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Gemnasium analyzer for GitLab Dependency-Scanning"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		AnalyzeAll:   true,
		Convert:      convert.Convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
